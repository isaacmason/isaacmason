# Hi there! I'm Isaac 👋

**Right now I am**

🍎. Working on grocery tech at Amazon

🔧. Making fun javascript things 

🐕. Trying to tire out my dog

**Find me here**

- [My Website](https://isaacmason.com/)

- [GitHub](https://github.com/isaac-mason)

- [LinkedIn](https://www.linkedin.com/in/isaac-charles-grant-mason/)
